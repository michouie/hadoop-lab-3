-- Here are the command I execute on Hive to do this lab 3. 

-- Creation of the database 
CREATE DATABASE mdengdb LOCATION '/user/mdeng/mdengdb';

use mdengdb;

-- This first table is an intermediate table where we store as textfile
-- Fields are delimited by the char ';'
-- The type array is used for the "gender" and "origin" field
-- and the delimiter caracter is ","
CREATE EXTERNAL TABLE prenoms (
    prenom STRING, 
    gender ARRAY<VARCHAR(10)>,
    origin ARRAY<VARCHAR(50)>, 
    version DOUBLE
    ) 
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\073' 
COLLECTION ITEMS TERMINATED BY '\054' 
STORED AS textfile 
LOCATION '/user/mdeng/prenoms';

-- In this table, the structure of the table is similar to the previous one,
-- However it is store as ORC
CREATE EXTERNAL TABLE prenoms_opt (
    prenom STRING,
    gender ARRAY<VARCHAR(10)>,
    origin ARRAY<VARCHAR(50)>,
    version DOUBLE
    )
ROW FORMAT DELIMITED
STORED AS ORC;

-- copy the data from the first table to the second one
INSERT INTO prenoms_opt SELECT * FROM prenoms;

-- Query 1
-- Count first name by origin
-- In this query, we use the function explode() to get the variable inside the array origin
-- Then we use the function ltrim() on it to remove the first whitespace from the string
-- We group by origin, and count the number of first name by origin
SELECT tmp2.anOrigin, count(*) 
FROM (
    SELECT ltrim(tmp.originNotTrim) AS anOrigin
    FROM (
        SELECT explode(origin) AS originNotTrim
        FROM prenoms_opt
        )tmp
    )tmp2 
GROUP BY tmp2.anOrigin;

-- Query 2
-- Count number of first name by number of origin
-- We use the size() function to get the number of origin per name
-- Then we group by the number of origins and we count the first name corresponding
SELECT nbOrigin, count(*) AS nbName 
FROM (
    SELECT prenom, size(origin) AS nbOrigin
    FROM prenoms_opt
    )tmp
GROUP BY nbOrigin;

-- Query 3
-- Proportion of male or female
-- We look for the proportion of mixte name or non-neutral gender name
-- I use the function size() to get the size of the "gender" array
-- Then I group by the number of gender
-- It returns 1 which stands for only male or only female name
-- It returns 2 which stands for a mixte name
-- These values are followed by the corresponding proportion
SELECT tmp.gend, count(tmp.gend)*100.0/T.total AS proportion 
FROM (
    SELECT size(gender) AS gend
    FROM prenoms_opt
    )tmp,
    (
    SELECT count(*) AS total
    FROM prenoms_opt
    )T 
GROUP BY tmp.gend, T.total;
